var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesla = {};




var fs  = require('fs');
var vulgarneBesede = fs.readFileSync('public/swearWords.txt').toString().split('\n');



exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', "");
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    //refreshUsers(socket, vzdevkiGledeNaSocket);
    obdelajPridruzitevKanalu(socket);
    obdelajPrivatSporocilo(socket);
    obdelajPridruzitevZascitenemKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, pass) {
          
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, users: makeLocalUsersList(socket,trenutniKanal[socket.id])});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  makeUsersList(socket,trenutniKanal[socket.id]);
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}


function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
    makeUsersList(socket,trenutniKanal[socket.id])
  });
}

function preveriVulgarneBesede(sporocilo) {
  var besede = sporocilo.split(" ");
  var noveBesede = sporocilo.split(" ");
  var novoSporocilo = "";
  
  for (var i = 0; i < besede.length; i++){
    //remove kebab... (extra signs at start or end of a word)
    for (var x = 0; x < besede[i].length; x++){
      if (besede[i].charAt(x) == ',' || besede[i].charAt(x) == '.' || besede[i].charAt(x) == '?' || besede[i].charAt(x) == '!' || besede[i].charAt(x) == ';' || besede[i].charAt(x) == ':'){
      }
      else{
        var novaBeseda = "";
        for (var y = x; y < besede[i].length; y++){
          novaBeseda += besede[i].charAt(y);
        }
        noveBesede[i] = novaBeseda;
        break;
      } 
    }
    for (var z = noveBesede[i].length; z > 0 ; z--){
      
      if (noveBesede[i].charAt(z-1) == ',' || noveBesede[i].charAt(z-1) == '.' || noveBesede[i].charAt(z-1) == '?' || noveBesede[i].charAt(z-1) == '!' || noveBesede[i].charAt(z-1) == ';' || noveBesede[i].charAt(z-1) == ':'){
      }
      else{
        var novaBeseda1 = "";
        for (var y1 = 0; y1 < z; y1++){
          novaBeseda1 += noveBesede[i].charAt(y1);
        }
        noveBesede[i] = novaBeseda1;
        //return novaBeseda1 + y1 + z + x + y + " " + noveBesede[i];
        break;
      } 
    }
    //noveBesede[i] = noveBesede[i].toLowerCase();
    //is the word vulgar?
    var wordIsVulgar = "NO";
    for (var xyz = 0; xyz < vulgarneBesede.length ; xyz++){
      
        if(noveBesede[i].toLowerCase().toString().trim() == vulgarneBesede[xyz].toLowerCase().toString().trim()){
          wordIsVulgar = "YES";
          //return noveBesede[i] + "test " + wordIsVulgar;
          break;
        }
    }
    //return xyz + noveBesede[i] + "test";
    //check if it is a vulgar word
    if(wordIsVulgar == "YES"){
      //remake the message with vulgar words switched
      var besedaTest = besede[i].toLowerCase();
      for (var j = 0; j < besede[i].length ; j++){
        if(besedaTest.charAt(j) == noveBesede[i].toLowerCase().charAt(0)){
          var prvaPolovica = besede[i].substring(0,j);
          var drugaPolovica = besede[i].substring(j+noveBesede[i].length);
          var spremenjenaBeseda;
          if (prvaPolovica.length > 0){
            spremenjenaBeseda = prvaPolovica;
          }
          else {
            spremenjenaBeseda = "";
          }
          for (var k = 0; k < noveBesede[i].length ; k++){
            spremenjenaBeseda += '*';
          }
          spremenjenaBeseda += drugaPolovica;
          besede[i] = spremenjenaBeseda;
          novoSporocilo += besede[i];
          break;
        }
      }
    }
    else{
        novoSporocilo += besede[i];
    }
    if(i+1 < besede.length){
      novoSporocilo += " ";
    }
    
  }
  
  return novoSporocilo;
}

function makeLocalUsersList(socket, kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var uporabnikiNaKanaluPovzetek='';
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    uporabnikiNaKanaluPovzetek += ' ' + vzdevkiGledeNaSocket[uporabnikSocketId];
  }
  return uporabnikiNaKanaluPovzetek;
}

function makeGlobalUsersList(socket, kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var uporabnikiNaKanaluPovzetek='';
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    uporabnikiNaKanaluPovzetek += ' ' + vzdevkiGledeNaSocket[uporabnikSocketId];
  }
  socket.broadcast.to(kanal).emit('trenutniLocalUporabniki',{users: uporabnikiNaKanaluPovzetek});
  return uporabnikiNaKanaluPovzetek;
}

function makeUsersList(socket, kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var uporabnikiNaKanaluPovzetek='';
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    uporabnikiNaKanaluPovzetek += ' ' + vzdevkiGledeNaSocket[uporabnikSocketId];
  }
  
  socket.broadcast.to(kanal).emit('trenutniLocalUporabniki',{users: uporabnikiNaKanaluPovzetek});
  socket.emit('trenutniLocalUporabniki',{users: uporabnikiNaKanaluPovzetek});
  return uporabnikiNaKanaluPovzetek;
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + preveriVulgarneBesede(sporocilo.besedilo)
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if (gesla[kanal.novKanal] !== undefined && gesla[kanal.novKanal] !== '' && gesla[kanal.novKanal] != null){
      //geslo se ne ujema 
      socket.emit('sporocilo', {
        besedilo: "Pridružitev v kanal " + kanal.novKanal + " ni bilo uspešno, ker je geslo napačno!"
      });
    }
    else {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
    }
  });
}

    
function obdelajPridruzitevZascitenemKanalu(socket) {
  socket.on('pridruzitevZahtevaSecure', function(kanal) {
    //preveri ce kanal obstaja
    for(var kanalTemp in io.sockets.manager.rooms){
      kanalTemp = kanalTemp.substring(1, kanalTemp.length);
      if(kanal.secKanal == kanalTemp){
        //in je zasciten
        if (gesla[kanal.secKanal] !== undefined && gesla[kanal.secKanal] !== '' && gesla[kanal.secKanal] != null) {
          if(gesla[kanal.secKanal] == kanal.pass){
          //poslji zahtevek za prestop kanala
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, kanal.secKanal);
          }
          else {
            //geslo se ne ujema 
            socket.emit('sporocilo', {
              besedilo: "Pridružitev v kanal " + kanal.secKanal + " ni bilo uspešno, ker je geslo napačno!"
            });
          }
        }
        else{
          // "Izbrani kanal <kanal> je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom."
          var sporocilo = "Izbrani kanal " + kanal.secKanal + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.";
          socket.emit('sporocilo', {
            besedilo: sporocilo
          });
        }
        return;
      }
    }
    
    //drugace naredi nov kanal  
    gesla[kanal.secKanal] = kanal.pass;
    //poslji zahtevek za prestop kanala
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.secKanal);
  });
}

function obdelajPrivatSporocilo(socket) {
  socket.on('privatSporocilo', function(vzdevekPrejemnika, privatSporocilo) {
    if (vzdevekPrejemnika.charAt(0) == "\"" && vzdevekPrejemnika.charAt(vzdevekPrejemnika.length-1) == "\""){
      //preveri ce obstaja (cez vse kanale, za vsak kanal cez vse uporabnike)
      for (var kanalTmp in io.sockets.manager.rooms){
        kanalTmp = kanalTmp.substring(1, kanalTmp.length);
        var uporabnikiNaKanalu = io.sockets.clients(kanalTmp);
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          if (vzdevekPrejemnika.substring(1,vzdevekPrejemnika.length-1) == vzdevkiGledeNaSocket[uporabnikSocketId] && uporabnikSocketId != socket.id){
            if (privatSporocilo[0].charAt(0) == "\"" && privatSporocilo[privatSporocilo.length-1].charAt(privatSporocilo[privatSporocilo.length-1].length-1) == "\""){
  
              //poslji sporocilo uporabniku
              io.sockets.socket(uporabnikSocketId).emit('privatSporociloPrejeto',{
                posiljatelj: vzdevkiGledeNaSocket[socket.id],
                sporocilo: preveriVulgarneBesede(privatSporocilo.join(" ").substring(1,privatSporocilo.join(" ").length-1))
              });
              //var msg = preveriVulgarneBesede("(zasebno za " + vzdevekPrejemnika.substring(1,vzdevekPrejemnika.length-1) + "): " + privatSporocilo.join(" ").substring(1,(privatSporocilo.join(" ")).length-1));
              io.sockets.socket(socket.id).emit('privatSporociloOdgovor',{
                uspesno: true,
                sporocilo: preveriVulgarneBesede("(zasebno za " + vzdevekPrejemnika.substring(1,vzdevekPrejemnika.length-1) + "): " + privatSporocilo.join(" ").substring(1,privatSporocilo.join(" ").length-1))
              });
            }
            else {
              //ERROR sporocilo nima ""
              socket.emit('privatSporociloOdgovor',{
                uspesno: false,
                sporocilo: 'Neznan ukaz.'
              });
            }
            return;
          }
        }
      }
      //ERROR uporabnik ne obstaja
      socket.emit('privatSporociloOdgovor',{
        uspesno: false,
        sporocilo: 'Sporočilo ' + privatSporocilo.join(" ").substring(1,privatSporocilo.join(" ").length-1) + ' uporabniku z vzdevkom ' +  vzdevekPrejemnika.substring(1,vzdevekPrejemnika.length-1) + ' ni bilo mogoče posredovati.'
      });
    }
    else {
      socket.emit('privatSporociloOdgovor',{
        uspesno: false,
        sporocilo: 'Neznan ukaz.'
      });
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var prejsniKanal = trenutniKanal[socket.id];
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    makeGlobalUsersList(socket, prejsniKanal);
  });
}