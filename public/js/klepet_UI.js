function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function preveriZaSmeskote(sporocilo) {
  
  var modSporocilo = sporocilo;
  
  modSporocilo = modSporocilo.replace( /:\(/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png "/>');
  modSporocilo = modSporocilo.replace( /\(y\)/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png "/>');
  modSporocilo = modSporocilo.replace( /:\)/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png "/>');
  modSporocilo = modSporocilo.replace( /:\*/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png "/>');
  modSporocilo = modSporocilo.replace( /;\)/g ,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png "/>');
  
  return modSporocilo;
}
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  console.log(["send", sporocilo]);
  var sistemskoSporocilo;
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    var userKanal = $('#kanal').text().split(" @ ");
    var kanal = userKanal[1];
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append($('<div style="font-weight: bold"></div>').html(preveriZaSmeskote(sporocilo)));
  }

  $('#poslji-sporocilo').val('');
}

function makeLocalUsersList(list){
  $('#seznam-users').empty();
  var users=list.split(' ');
  for (var i in users) {
    $('#seznam-users').append(divElementEnostavniTekst(users[i]));
  }
  socket.on('trenutniLocalUporabniki', function(uporabniki) {
    makeLocalUsersList(uporabniki.users);
 });
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      var besede = $('#kanal').text().split(" @ ");
      var naslov = rezultat.vzdevek + " @ " + besede[1];
      $('#kanal').text(naslov);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    
  });
  
  socket.on('privatSporociloOdgovor', function(rezultat) {
    var sporocilo = rezultat.sporocilo ;
    if (rezultat.uspesno) {
      var novoSporocilo = $('<div style="font-weight: bold"></div>').html(preveriZaSmeskote(sporocilo));
      $('#sporocila').append(novoSporocilo);
    } else {
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
    }
  });
//rezultat.posiljatelj +  
  socket.on('privatSporociloPrejeto', function(rezultat) {
    console.log(rezultat);
    var sporocilo = rezultat.posiljatelj + " (zasebno): " + preveriZaSmeskote(rezultat.sporocilo);
    var novoSporocilo = $('<div style="font-weight: bold"></div>').html(sporocilo);
    console.log(sporocilo);
    $('#sporocila').append(novoSporocilo);
  });
  
  socket.on('pridruzitevOdgovor', function(rezultat) {
    var besede = $('#kanal').text().split(" @ ");
    var naslov = besede[0] + " @ " + rezultat.kanal;
    $('#kanal').text(naslov);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    makeLocalUsersList(rezultat.users);
  });


  socket.on('sporocilo', function (sporocilo) {
    var novoSporocilo = $('<div style="font-weight: bold"></div>').html(preveriZaSmeskote(sporocilo.besedilo));
    $('#sporocila').append(novoSporocilo);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });

  return false;
});