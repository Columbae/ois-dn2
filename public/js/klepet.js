var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      if(besede[0].charAt(0) == '"' && besede[0].charAt(besede[0].length-1) == '"'){
        
        if (besede[1].charAt(0) == '"' && besede[1].charAt(besede[1].length-1) == '"') {
          var secKanal = besede[0].substring(1, besede[0].length-1);
          var pass = besede[1].substring(1, besede[1].length-1);
          this.socket.emit('pridruzitevZahtevaSecure', {
            secKanal: secKanal,
            pass: pass
          });
        }
        else {
          var kanal1 = besede.join(' ');
          this.spremeniKanal(kanal1);
        }
      }
      else {
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var vzdevekPrejemnika = besede.shift();
      this.socket.emit('privatSporocilo', vzdevekPrejemnika, besede);
      break;
    default:
      sporocilo = 'Neznan ukaz1.';
      break;
  }

  return sporocilo;
};